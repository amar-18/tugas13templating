@extends('adminlte.master')

@section('content')
        <div class="mt-5 ml-5 mb-5 mr-5">
        <form action="/pertanyaan/{{$pertanyaan->id}}" method="POST">
            @csrf
            @method('put')
            <div class="form-group">
                <label for="judul">Judul</label>
                <input type="text" class="form-control" name="judul" id="judul" value="{{$pertanyaan->judul}}" placeholder="Masukkan judul">
                @error('judul')
                    <div class="alert alert-danger">
                        {{ $message }}
                    </div>
                @enderror
            </div>
            <div class="form-group">
                <label for="isi">Isi</label>
                <textarea class="form-control" name="isi" cols="30" row="10" id="isi" >
                {{$pertanyaan->isi}}
                </textarea>
                @error('isi')
                    <div class="alert alert-danger">
                        {{ $message }}
                    </div>
                @enderror
            </div>
            <button type="submit" class="btn btn-primary">Edit</button>
        </form>
        </div>
@endsection