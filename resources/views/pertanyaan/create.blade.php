@extends('adminlte.master')

@section('content')
        <div class="mt-5 ml-5 mb-5 mr-5">
        <form action="/pertanyaan" method="POST">
            @csrf
            <div class="form-group">
                <label for="judul">Judul</label>
                <input type="text" class="form-control" name="judul" id="judul" placeholder="Masukkan judul">
                @error('judul')
                    <div class="alert alert-danger">
                        {{ $message }}
                    </div>
                @enderror
            </div>
            <div class="form-group">
                <label for="isi">Isi</label>
                <textarea class="form-control" name="isi" cols="30" row="10" id="isi" placeholder="Masukkan isi">
                </textarea>
                @error('isi')
                    <div class="alert alert-danger">
                        {{ $message }}
                    </div>
                @enderror
            </div>
            <button type="submit" class="btn btn-primary">Tambah</button>
        </form>
        </div>
@endsection