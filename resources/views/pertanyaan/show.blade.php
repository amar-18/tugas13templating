@extends('adminlte.master')

@section('content')
<div class="mt-3 ml-3 mr-3 mb-3">
<h4>{{$pertanyaan->judul}}</h4>
<p>{{$pertanyaan->isi}}</p>
<a href="/pertanyaan" class="btn btn-danger my-1">Back</a>
</div>
@endsection